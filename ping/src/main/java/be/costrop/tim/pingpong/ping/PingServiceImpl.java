package be.costrop.tim.pingpong.ping;

import be.costrop.tim.pingpong.shared.internal.PingService;
import be.costrop.tim.pingpong.shared.internal.PongService;
import be.costrop.tim.pingpong.shared.internal.TableLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@Service
public class PingServiceImpl implements PingService {
    public static final Logger LOGGER = LoggerFactory.getLogger(PingService.class);
    private boolean gameHasStarted = false;

    private final PongService pongService;

    public PingServiceImpl(PongService pongService) {
        this.pongService = pongService;
    }

    @Override
    public void hitToPing(Integer number) {
        LOGGER.info("Received ball {}", number);

        try {
            TimeUnit.SECONDS.sleep(3);

            pongService.hitToPong(serveBall());

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(fixedDelay = 3000L)
    private void checkPongIsReadyYet() {
        if(!gameHasStarted) {
            try {
                final var pongIsReady = CompletableFuture.supplyAsync(pongService::ready).get(2L, TimeUnit.SECONDS);
                if (pongIsReady) {
                    pongService.hitToPong(serveBall());
                    gameHasStarted = true;
                }
            } catch (Exception e) {
                LOGGER.info("pong is not ready yet.", e);
            }
        }
    }

    private Integer serveBall() {
        return ThreadLocalRandom.current().nextInt(0, 10);
    }

    @Override
    public boolean ready() {
        return true;
    }
}
