package be.costrop.tim.pingpong.pong;

import be.costrop.tim.pingpong.shared.internal.PingService;
import be.costrop.tim.pingpong.shared.internal.PongService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.cxf.BusFactory;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.feature.LoggingFeature;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.core.MediaType;
import java.util.Collections;

@Configuration
public class PongConfiguration {

    @Value("${cxf.jaxrs.client.address}")
    private String serviceLocation;

    @Bean
    public JacksonJsonProvider jacksonJsonProvider(ObjectMapper objectMapper) {
        return new JacksonJsonProvider(objectMapper);
    }

    @Bean
    public JacksonJaxbJsonProvider jacksonJaxbJsonProvider() {
        return new JacksonJaxbJsonProvider();
    }

    @Bean
    public PingService pingService(SpringBus bus, JacksonJsonProvider jacksonJsonProvider) {
        BusFactory.setDefaultBus(bus);
        final PingService client = JAXRSClientFactory.create(serviceLocation, PingService.class, Collections.singletonList(jacksonJsonProvider));

        WebClient.client(client)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE);
        return client;
    }

}
