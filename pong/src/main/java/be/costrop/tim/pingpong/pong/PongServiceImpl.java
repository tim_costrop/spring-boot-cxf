package be.costrop.tim.pingpong.pong;

import be.costrop.tim.pingpong.shared.internal.PingService;
import be.costrop.tim.pingpong.shared.internal.PongService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@Service
public class PongServiceImpl implements PongService {

    public static final Logger LOGGER = LoggerFactory.getLogger(PongService.class);

    private final PingService pingService;

    public PongServiceImpl(PingService pingService) {
        this.pingService = pingService;
    }

    @Override
    public void hitToPong(Integer ball) {
        LOGGER.info("Received ball {}", ball);

        try {
            TimeUnit.SECONDS.sleep(3);

            pingService.hitToPing(ThreadLocalRandom.current().nextInt(0, 10));

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean ready() {
        return true;
    }
}
