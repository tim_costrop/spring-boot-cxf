package be.costrop.tim.pingpong.shared.internal;

public enum TableLocation {
    LEFT_BEHIND,
    LEFT_FRONT,
    RIGHT_BEHIND,
    RIGHT_FRONT,
    ;

    /*      |----------------------------|
    *       | LEFT_BEHIND | RIGHT_BEHIND |
    *       |-------------|--------------|
    *       |  LEFT_FRONT |  RIGHT_FRONT |
    *       ############## NET ##################
    * */
}
