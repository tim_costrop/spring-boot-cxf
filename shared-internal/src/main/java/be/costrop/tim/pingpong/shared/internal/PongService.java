package be.costrop.tim.pingpong.shared.internal;

import org.springframework.http.MediaType;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/pong")
public interface PongService {
    @POST
    @Path("/paddle")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    void hitToPong(Integer number);

    @GET
    @Path("/ready")
    boolean ready();
}
