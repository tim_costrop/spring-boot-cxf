package be.costrop.tim.pingpong.shared.internal;

import org.apache.cxf.annotations.Provider;
import org.springframework.http.MediaType;

import javax.ws.rs.*;

@Path("/ping")
@Consumes(MediaType.APPLICATION_JSON_VALUE)
@Produces(MediaType.APPLICATION_JSON_VALUE)
public interface PingService {

    @POST
    @Path("/paddle")
    void hitToPing(Integer number);

    @GET
    @Path("/ready")
    boolean ready();
}
